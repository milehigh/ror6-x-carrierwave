class Post < ApplicationRecord
    include CarrierWave::MiniMagick

    mount_uploader :image, ImageUploader
    belongs_to :category
    validates :title, presence: true
    validates :image, presence: true
    # validates :category_id, inclusion: {in: Category.all.pluck(:id)}
    validate :check_image_dimensions

    # ------------------------------------------------------------------
    # Public Instance Methods
    # ------------------------------------------------------------------
    def check_image_dimensions
        unless geometry.nil?
            if geometry[:width] < 50 || geometry[:height] < 50 || geometry[:width] > 1000 || geometry[:height] > 1000
                errors.add(:image, ' must be between 50x50 and 1000x1000')
            end
        end

        # for debug
        p '--------------------------------------------------'
        p geometry
        p '--------------------------------------------------'
    end

    def geometry
        @geometry ||= _geometry
    end

    private
    # ------------------------------------------------------------------
    # Private Instance Methods
    # ------------------------------------------------------------------
    def _geometry
        if image.current_path
            img = MiniMagick::Image.open(image.current_path)
            if img
                { width: img[:width], height: img[:height] }
            end
        end
    end

end
