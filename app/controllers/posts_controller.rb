class PostsController < ApplicationController
    layout "application"

    def index
        @posts = Post.all
    end
    
    def add
        @post = Post.new
    end

    def create
        post = Post.new post_params
        post.save!
        redirect_to root_path and return
    end

    private
    def post_params
      params.require(:post).permit(:title, :image, :category_id)
    end
end
